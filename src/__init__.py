from os import environ
from hashlib import md5
import redis
import requests

PUSHOVER_URL = 'https://api.pushover.net/1/messages.json'

def update_redis(redis_url, key, value, verbose):
    r = redis.Redis.from_url(redis_url)
    redis_val = r.get(key)
    hashed_val = md5(str(value).encode('utf-8')).hexdigest()
    if not redis_val:
        if verbose: print('Setting redis value')
        r.set(key, hashed_val)
        return True
    else:
        if verbose > 1:
            print('{val1} != {val2}'.format(
                val1=hashed_val,
                val2=redis_val
            ))
        if hashed_val != redis_val.decode('utf-8'):
            if verbose: print('Value mismatch, updating redis value')
            r.set(key, hashed_val)
            return True
        else:
            if verbose > 1: print('No redis update required')
            return False


def send_alert(key, token, msg):
    data = {
        'token': token,
        'user': key,
        'message': msg
    }

    if 'MODERATOR_ALERT_URL' in environ:
        data['url'] = environ.get('MODERATOR_ALERT_URL')
        data['url_title'] = environ.get(
            'MODERATOR_ALERT_URL_TITLE', 'Manage'
        )

    response = requests.post(
        PUSHOVER_URL,
        data=data
    )

    if response.status_code != 200:
        raise RuntimeError('Pushover {code}: {body}'.format(
            code=response.status_code,
            body=response.content
        ))


class BaseProbe(object):

    def __init__(self, url, token, verbose):
        self.url = url
        self.path = '/'
        self.params = {}
        self.headers = {
            'Authorization': 'Bearer {}'.format(token)
        }
        self.alert = False
        self._response = None
        self.msg = ''
        self.verbose = verbose

    def run(self):
        self.response = requests.get(
            '{}/api/v1/{}'.format(self.url, self.path),
            params=self.params,
            headers=self.headers
        )

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, val):
        self._response = val
        if val.status_code != 200:
            self.alert = True
            self.msg = '{code}: {reason}: {content}'.format(
                code=self.response.status_code,
                reason=self.response.reason,
                content=self.response.content
            )


class AccountsProbe(BaseProbe):

    def __init__(self, url, token, verbose):
        super().__init__(url, token, verbose)
        self.path = 'admin/accounts'
        self.params = {
            'pending': True
        }

    @BaseProbe.response.setter
    def response(self, val):
        self._response = val
        if self._response.status_code == 200:
            accounts = self.response.json()
            accounts_num = len(accounts)
            if accounts_num > 0:
                self.alert = True
                self.msg = 'Accounts pending: {}\n\n'.format(accounts_num)
                for a in accounts:
                    self.msg += '{email}: {request}\n'.format(
                        email=a['email'],
                        request=a['invite_request']
                    )
        else:
            self.alert = True
            self.msg = '{code}: {reason}: {content}'.format(
                code=self.response.status_code,
                reason=self.response.reason,
                content=self.response.content
            )


class ReportsProbe(BaseProbe):

    def __init__(self, url, token, verbose):
        super().__init__(url, token, verbose)
        self.path = 'admin/reports'
        self.params = {
            'resolved': False
        }

    @BaseProbe.response.setter
    def response(self, val):
        self._response = val
        if self._response.status_code == 200:
            reports_count = 0
            reports = self.response.json()
            for report in reports:
                if report['action_taken']:
                    continue
                reports_count += 1
            if reports_count > 0:
                self.alert = True
                self.msg = 'Reports pending: {}'.format(reports_count)
        else:
            self.alert = True
            self.msg = '{code}: {reason}: {content}'.format(
                code=self.response.status_code,
                reason=self.response.reason,
                content=self.response.content
            )

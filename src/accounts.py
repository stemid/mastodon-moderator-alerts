import json
import atexit
from sys import exit
from os import stat
from fnmatch import fnmatch
from datetime import datetime

import gcld3
import click
import requests
from socket import gethostbyaddr
from ipwhois import IPWhois
from mastodon import Mastodon
from flag import flag


def write_usercache(cache, filename):
    with open(filename, 'w') as cache_file:
        json.dump(cache, cache_file)

def get_flag(country_code):
    return flag(country_code)

def get_hostname(ip):
    try:
        hostname = gethostbyaddr(ip)[0]
    except:
        hostname = 'NA'
    return hostname

def get_asninfo(ip):
    obj = IPWhois(ip)
    try:
        whois = obj.lookup_rdap(depth=1)
    except:
        return {
            'asn_country_code': ''
        }
    return {
        'asn_country_code': whois['asn_country_code'],
        'asn_description': whois['asn_description'],
        'entities': whois['entities']
    }

def pushover_alert(cache, user, app, error):
    if 'last_alert' in cache:
        cur_time = datetime.now()
        last_alert = datetime.fromisoformat(cache['last_alert'])
        diff = cur_time - last_alert
        hours = diff.total_seconds()/(60*60)
        if hours <= 1:
            return False

    res = requests.post(
        'https://api.pushover.net/1/messages.json',
        data={
            'token': app,
            'user': user,
            'device': 'pixel3',
            'title': 'Mastodon accounts listing failed',
            'message': error
        }
    )
    if res.status_code == 200:
        cache['last_alert'] = datetime.now().isoformat()

@click.command()
@click.option(
    '--url',
    metavar='MASTODON_URL',
    envvar='MASTODON_URL',
    help='Mastodon instance base url'
)
@click.option(
    '--mastodon-token',
    metavar='MASTODON_ACCESS_TOKEN',
    envvar='MASTODON_ACCESS_TOKEN',
    help='Mastodon access token'
)
@click.option(
    '--pushover-user', 'po_user',
    metavar='PUSHOVER_USER_KEY',
    envvar='PUSHOVER_USER_KEY',
    help='Pushover User key'
)
@click.option(
    '--pushover-app', 'po_app',
    metavar='PUSHOVER_APP_TOKEN',
    envvar='PUSHOVER_APP_TOKEN',
    help='Pushover App token'
)
@click.option(
    '-A', '--filter-by-asn', 'filter_country',
    metavar='ASN_COUNTRY_CODE',
    help='Filter accounts by ASN country code (example: SE)'
)
@click.option(
    '-U', '--filter-by-name', 'filter_username',
    metavar='USERNAME',
    help='Filter accounts by username, will have wildcards appended to it so only the first part is necessary'
)
@click.option(
    '-D', '--filter-by-descr', 'filter_descr',
    metavar='STRING',
    help='Filter by description text'
)
@click.option(
    '-L', '--filtrer-by-lang', 'filter_lang',
    metavar='LANGUAGE_CODE',
    help='Filter by specifying BCP-47 language code of description text'
)
@click.option(
    '--asn-cache', 'cache_filename',
    type=click.Path(exists=False, readable=True, writable=True),
    default='cache.json',
    help='Whois lookup cache to avoid rate limiting'
)
@click.option(
    '--approve',
    is_flag=True,
    help='Approve all listed accounts'
)
@click.option(
    '--reject',
    is_flag=True,
    help='Reject all listed accounts'
)
@click.option(
    '--limit',
    default=0,
    type=int,
    help='Limit number of pending accounts iterated to this'
)
@click.option('-v', '--verbose', count=True)
def accounts(url, mastodon_token, po_user, po_app, filter_country,
             filter_username, filter_descr, filter_lang, cache_filename,
             approve, reject, limit, verbose):
    format_str = '{username}, {email}, {ip}(\033[1m{hostname}\033[0m), {country} {flag}\n{request}\n'

    m = Mastodon(
        api_base_url=url,
        access_token=mastodon_token
    )

    # Init asn cache dict
    cache = {}
    # Load cache from file if file size is larger than 0
    try:
        if stat(cache_filename).st_size > 0:
            with open(cache_filename, 'r') as cache_file:
                cache = json.load(cache_file)
    except FileNotFoundError:
        pass

    atexit.register(write_usercache, cache, cache_filename)

    count=0
    approved=0
    rejected=0

    try:
        pending = m.admin_accounts(status='pending')
    except Exception as e:
        if po_user and po_app:
            pushover_alert(cache, po_user, po_app, str(e))
        print(str(e))
        exit(1)

    for account in pending:
        username = account['username']
        # Check cache for IP to avoid hitting whois services
        account_ip = account['ip']['ip']
        request = account['invite_request']
        if not account['invite_request']:
            request = 'None'

        if account_ip in cache:
            userinfo = cache[account_ip]
        else:
            userinfo = get_asninfo(account_ip)
            if len(userinfo) > 0:
                cache[account_ip] = userinfo

        if 'gcld3_language' not in userinfo and filter_lang:
            detector = gcld3.NNetLanguageIdentifier(min_num_bytes=5, max_num_bytes=1000)
            lang_result = detector.FindLanguage(text=request)
            userinfo['gcld3_language'] = lang_result.language
            userinfo['gcld3_probability'] = lang_result.probability

        if filter_username:
            if not fnmatch(username, filter_username+'*'):
                continue

        if filter_country:
            if filter_country != userinfo['asn_country_code']:
                continue

        if filter_descr:
            if not request.startswith(filter_descr):
                continue

        if filter_lang:
            if userinfo['gcld3_language'] != filter_lang:
                continue

        count += 1
        print(format_str.format(
            username=username,
            email=account['email'],
            ip=account_ip,
            hostname=get_hostname(account_ip),
            country=userinfo['asn_country_code'],
            flag=get_flag(userinfo['asn_country_code']),
            request=request
        ))

        if approve:
            m.admin_account_approve(account['id'])
            approved += 1
        if reject:
            m.admin_account_reject(account['id'])
            rejected += 1

        if limit > 0 and count >= limit:
            break

    if approved > 0 or rejected > 0:
        print('approved {}/{} accounts, rejected {}/{} accounts'.format(
            approved,
            count,
            rejected,
            count
        ))
    else:
        print('listed {} accounts'.format(count))


if __name__ == '__main__':
    exit(accounts(None))


import os
from . import create_app, load_config

load_config()
application = create_app()

if __name__ == '__main__':
    application.run(
        host=os.getenv('APP_LISTEN_HOST', '0.0.0.0'),
        debug=bool(os.getenv('APP_DEBUG', True))
    )

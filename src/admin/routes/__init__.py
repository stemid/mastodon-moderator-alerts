from os import environ
from functools import wraps
import requests
from flask import Blueprint, render_template, request, jsonify, redirect
from src import AccountsProbe


class AdminAccounts(object):
    def __init__(self, url, token, accounts):
        self.url = url
        self.path = 'admin/accounts'
        self.headers = {
            'Authorization': 'Bearer {}'.format(token)
        }
        self.accounts = accounts

    def approve(self):
        res = []
        for account_id in self.accounts:
            res.append(requests.post(
                '{}/api/v1/{}/{}/approve'.format(
                    self.url,
                    self.path,
                    account_id
                ),
                headers=self.headers
            ))
        return res

    def reject(self):
        res = []
        for account_id in self.accounts:
            res.append(requests.post(
                '{}/api/v1/{}/{}/reject'.format(
                    self.url,
                    self.path,
                    account_id
                ),
                headers=self.headers
            ))
        return res

def auth_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'token' not in request.args:
            return jsonify({
                'status': 'error'
            }), 401
        if environ.get('MODERATOR_AUTH_TOKEN') != request.args['token']:
            return jsonify({
                'status': 'access denied'
            }), 401
        return f(*args, **kwargs)
    return decorated_function

moderator_bp = Blueprint(
    'moderator',
    __name__,
    template_folder='templates',
    static_folder='static'
)

@moderator_bp.route('/')
@moderator_bp.route('/accounts', methods=['GET', 'POST'])
@auth_required
def accounts():
    if request.method == 'POST':
        items = request.form.getlist('items')
        aa = AdminAccounts(
            environ.get('MASTODON_URL'),
            environ.get('MASTODON_ACCESS_TOKEN'),
            items
        )
        if request.form['submit'] == 'approve':
            results = aa.approve()
        if request.form['submit'] == 'reject':
            results = aa.reject()

        return redirect('/accounts?token={}'.format(
            environ.get('MODERATOR_AUTH_TOKEN')
        ))

    ap = AccountsProbe(
        environ.get('MASTODON_URL'),
        environ.get('MASTODON_ACCESS_TOKEN')
    )
    ap.run()
    accounts = ap.response.json()
    if 'error' in accounts:
        accounts = []
    return render_template(
        'accounts.html',
        accounts=accounts,
        token=environ.get('MODERATOR_AUTH_TOKEN')
    )

@moderator_bp.route('/reports')
@auth_required
def reports():
    return render_template(
        'reports.html',
        token=environ.get('MODERATOR_AUTH_TOKEN')
    )


@moderator_bp.route('/liveness')
def liveness():
    return jsonify({
        'status': 'live'
    })

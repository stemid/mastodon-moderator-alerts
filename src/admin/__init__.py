import os
import atexit
from pathlib import Path
from flask import Flask
from dotenv import load_dotenv
from . import routes

APP_NAME = 'mastodon-moderator'


def load_config():
    env_path = Path('.') / '.env'
    if 'FLASK_ENV' in os.environ:
        env_path = '{path}.{env}'.format(
            path=env_path,
            env=os.getenv('FLASK_ENV')
        )
    load_dotenv(
        dotenv_path=env_path
    )


def create_app():
    """
    Create flask app object instance
    """
    app = Flask(__name__)
    register_blueprints(app)

    return app


def register_blueprints(app):
    """
    Register Flask blueprints necessary in production deployment.
    """
    app.register_blueprint(routes.moderator_bp)
    return None


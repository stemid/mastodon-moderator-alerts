from sys import exit
from os import environ
from pathlib import Path

import click
from dotenv import load_dotenv
from . import send_alert, update_redis
from . import AccountsProbe, ReportsProbe


def load_config():
    env_path = Path('.') / '.env'
    if 'FLASK_ENV' in environ:
        env_path = '{path}.{env}'.format(
            path=env_path,
            env=environ.get('FLASK_ENV')
        )
    load_dotenv(
        dotenv_path=env_path
    )

load_config()

@click.command()
@click.option(
    '--url',
    metavar='MASTODON_URL',
    envvar='MASTODON_URL',
    help='Mastodon instance base url'
)
@click.option(
    '--mastodon-token',
    metavar='MASTODON_ACCESS_TOKEN',
    envvar='MASTODON_ACCESS_TOKEN',
    help='Mastodon access token'
)
@click.option(
    '--pushover-user-key', 'pushover_key',
    metavar='PUSHOVER_USER_KEY',
    envvar='PUSHOVER_USER_KEY',
    help='Pushover user key'
)
@click.option(
    '--pushover-app-token', 'pushover_token',
    metavar='PUSHOVER_APP_TOKEN',
    envvar='PUSHOVER_APP_TOKEN',
    help='Pushover app token'
)
@click.option(
    '--redis-url',
    metavar='REDIS_URL',
    envvar='REDIS_URL',
    default=None,
    help='Optional Redis URL (example: redis://localhost:6379/0)'
)
@click.option('-v', '--verbose', count=True)
def moderator(url, mastodon_token, pushover_key, pushover_token,
              redis_url, verbose):
    probes = []
    probes.append(AccountsProbe(url, mastodon_token, verbose))
    probes.append(ReportsProbe(url, mastodon_token, verbose))

    for probe in probes:
        if verbose > 1:
            print('Running probe: {probe_name}'.format(
                probe_name=str(probe)
            ))
        try:
            probe.run()
        except Exception as e:
            if verbose > 2:
                raise
            if verbose: print(str(e))
            continue

        response = probe.response

        if verbose > 2:
            print(response.status_code, response.content)

        updated = False
        if redis_url and len(probe.msg) > 0:
            if verbose:
                print('Updating redis ({redis_url})'.format(
                    redis_url=redis_url
                ))
            updated = update_redis(
                redis_url,
                probe.path,
                probe.msg,
                verbose
            )
            if not updated:
                return 0

        if probe.alert and updated:
            if verbose: print('Sending alert!')
            send_alert(pushover_key, pushover_token, probe.msg)


if __name__ == '__main__':
    exit(moderator(None))


# Mastodon Moderation alerts using Pushover.net

This alerts me when new accounts are awaiting approval, or new reports are created.

## Run it

This should run in a cron job.

    pip install -r requirements.txt
    python -m src.moderator --help

You can use environment variables for each CLI option, as [shown below](#environment-settings).

## Run admin GUI

Runs a web gui for when you can't access the Mastodon admin GUI through your phone. The idea is you can send a link to the web GUI with your pushover alert and quickly respond to pending accounts.

    python -m src.admin.run

## Environment settings

Create a file called .env.dev and set ``FLASK_ENV=dev`` to use that file. By default the .env file is used.

* ``MASTODON_URL`` - Your instance URL, used to reach the API.
* ``MASTODON_ACCESS_TOKEN`` - Created in the Mastodon settings under Development, Apps.
* ``PUSHOVER_USER_KEY`` - Get one from [pushover.net](https://pushover.net).
* ``PUSHOVER_APP_TOKEN`` - Same.
* ``MODERATOR_AUTH_TOKEN`` - Secret token that gives you access to the admin GUI, by setting the token parameter in the url. (moderator.tld?token=secret)
* ``MODERATOR_ALERT_URL`` - If set, appended to pushover alert as [url property](https://pushover.net/api).
* ``MODERATOR_ALERT_URL_TITLE`` - If ``MODERATOR_ALERT_URL_TITLE`` is set this value is also set in the alert as [url_title property](https://pushover.net/api).

## Deploy to kubernetes

1. Get all the necessary tokens, mastodon access token, pushover user key and pushover app token.
2. Create your own overlay in ``deploy/kustomize/overlays`` matching your own private branch name.
3. Create a CI File-type variable called ``MODERATOR_ENV`` with environment settings for moderator.py, can look like this;
  ```
  MASTODON_URL=https://my.mastodon.instance
  MASTODON_ACCESS_TOKEN=xxx
  PUSHOVER_USER_KEY=xxx
  PUSHOVER_APP_TOKEN=xxx
  ```
4. Create a CI variable called ``K8S_NAMESPACE`` with the namespace for your mastodon deploy.
5. Create a CI variable called ``KUBECONFIG`` with the kubeconfig.yaml of a CI user with the ClusterRole edit in that namespace.
6. Create your own cronjob.yaml in the overlay and mount your secret environment variables, see example in [sydit branch](https://gitlab.com/stemid/mastodon-moderator-alerts/-/blob/sydit-env/deploy/kustomize/overlays/sydit-env/cronjob.yaml).

# Simple moderation of pending accounts

Besides the containerized pushover alerting service I also added the src/accounts.py script which is a much simpler CLI tool to approve or reject pending accounts. It just adds features I was missing from the Mastodon web GUI, like showing a reverse PTR lookup of each user's IP, and the country from the IP's ASN info. This is how I decide whether or not I want to approve the account on my localized Swedish instance.

## Setup

* Uses two environment variables, ``MASTODON_URL`` and ``MASTODON_ACCESS_TOKEN``. I personally use [direnv](https://github.com/direnv/direnv) to run this locally.
* Relies on the Google cld3 library for language detection.

### Language detection dependencies for Fedora

    sudo dnf install protobuf-compiler protobuf-devel

## Run

### List accounts pending

    python -m src.accounts

### Approve or reject by ASN country code

    python -m src.accounts --filter-by-asn SE --approve

### Reject by description

    python -m src.accounts -D 'None' --reject

#!/usr/bin/env bash

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

from_tag=docker.io/alpine:3.14

set -x

# Use some environment variables here so we can provide them in CI pipelines.
container="$(buildah from $from_tag)"
project_name="${PROJECT_NAME:-mastodon-moderator-alerts}"
registry_image="${REGISTRY_IMAGE:-$1}"

buildah config --label maintainer="Stefan Midjich" "$container"
buildah config --cmd '' "$container"
buildah config --entrypoint '["/usr/bin/python3", "-m", "src.moderator"]' "$container"
buildah config --workingdir /src "$container"

buildah copy "$container" src '/src/src'
buildah copy "$container" requirements.txt
buildah run "$container" apk -U update
buildah run "$container" apk --update-cache add curl bash python3 py3-pip
buildah run "$container" pip install -r requirements.txt

echo "$0: buildah commit"
buildah commit "$container" "$project_name:latest"
